// Author : Fabrice DAO
// Date 24/01/19


/*

Afin de résoudre l'exercice j'ai procédé façon clinteastwood ( le bon, la brute et le hackeur ).
J'ai opté pour déchiffré le hash de passer par une méthode de dictionnaire généré à la volée
pour tester toutes les combinaisons possible jusqu’à trouver la chaîne correspondante.

- Le bon : Cette méthode permet une grande souplesse pour la résolution du problème,
en effet elle est non dépendante du jeux de caractères () ainsi que de la fonction de hash.

- La brute : Cette solution est très gourmande en terme de calcul et de temps nécessaire pour tester toutes combinaisons.
Techniquement il faudrait avec un ordinateur 'moyenne-gamme' environ une semaine de runtime pour tester un dictionnaire allant de aaaaaaaa à wwwwwwww.

- Et le hackeur : N'ayant pas une semaine devant moi j'explique plus bas comment j'ai utilisé une fonction intermédiaire
afin de réduire considérablement la plage à tester. Ainsi pour la démo, nous testerons la fourchette suivante : de 'cameaaaa' à 'camewwww'
pour un runtime de 5 seconde.

*/


// Fonction de hash fournie par doyouno

function hash( str )
{
    let h = 7;

    let letters = "acdegilmnoprstuw";

    for( var i = 0; i < str.length; i++)
    {

        h = (h * 37 + letters.indexOf(str[i]));
    }

    return h;
}



/*

La fonction ci-dessous permet de générer un dictionnaire à la volée.
En entrée, on lui renseigne une chaîne et en sortie cette dernière génère la combinaison suivante.
Pour cela, elle convertie la chaine en digit et l'incrémente, puis la reconvertie en chaîne de nouveau.
eg : avec un set de lettre restraint abc.

input : aaa -> [0,0,0] -incrementation-> [0,0,1] -> output : aab
input : aab -> [0,0,1] -incrementation-> [0,0,2] -> output : aac
input : aac -> [0,0,2] -incrementation-> [0,1,0] -> output : aba

ect, ect ... jusqu'à ccc.

 */

function strNextPattern ( currentStr )
{
    //Set de lettres imposées par l'exercice
    let letters = "acdegilmnoprstuw";

    let digitArray = [];

    //Convertion de la chaine en entrée vers un tableau de digits
    for( var i = 0; i < currentStr.length; i++)
    {
        digitArray[i] = letters.indexOf(currentStr[i]);
    }

    //Incrémentation du tableau de digits
    for( var y = digitArray.length - 1; y > 0; y--)
    {
        if ( digitArray[y] >= (letters.length-1) )
        {
            digitArray[y] = 0;

            digitArray[y - 1] = digitArray[y - 1] + 1;

            if( digitArray[y-1] <= (letters.length-1) ) break;
        }
        else
        {
            digitArray[y] = digitArray[y] + 1;
            break;
        }
    }

    let outputStr = '';

    //Reconvertion du tableau de digits incrémenté vers une chaîne.
    for (let digit of digitArray)
    {
        outputStr += letters[digit];
    }

    return outputStr;
}


//Petite fonction utilitaire copiée de stackoverflow afin de convertir les milliseconds vers une chaîne lisible par l'homme, type hh:mm:ss
// https://stackoverflow.com/questions/19700283/how-to-convert-time-milliseconds-to-hours-min-sec-format-in-javascript
function msToTime(duration)
{
    var milliseconds = parseInt((duration % 1000) / 100),
        seconds = parseInt((duration / 1000) % 60),
        minutes = parseInt((duration / (1000 * 60)) % 60),
        hours = parseInt((duration / (1000 * 60 * 60)) % 24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}


/*
Ceci est la fonction principale nécessitant en entrée une plage de caractères.
Basiquement, une simple boucle while qui teste toutes combinaisons jusqu'à trouver la correspondance ou avoir atteint la fin de la plage fournie en entrée.
 */
function allPossibleStr ( startStr, endStr, hashToFind )
{

    // timestamp du startup
    let startTime = new Date();

    // On test la 1er combinaison
    var hashOuput = hash(startStr);
    console.log(hashOuput);

    // On crée une variable intermédiaire qui contiendra les combinaisons suivantes
    var strTest = startStr;
    console.log(strTest);

    // On test toutes les combinaisons jusqu'à trouver la correspondance ou bien toucher la fin de plage
    while (strTest !== endStr && hashOuput !== hashToFind )
    {
        //On passe à la combinaison suivante
        strTest = strNextPattern(strTest);
        console.log(strTest);

        //On hash la combinaison courante
        hashOuput = hash(strTest);
        console.log(hashOuput);
    }

    //On affiche le résultat final
    console.log( 'Hash [ ' + hashOuput + ' ] match to { ' + strTest + ' } string');

    // Timestamp de fin de traitement
    let endTime = new Date();
    //Calcul de la durée du traitement
    let timeDiff = endTime - startTime;
    console.log(msToTime(timeDiff));
}

// On lance le traitement sur toutes combinaisons ( env 1 semaine )
// allPossibleStr( 'aaaaaaaa', 'wwwwwwww', 24682779393128);

//On lance le traitement sur une plage réduite ( env 5 sec )
allPossibleStr( 'camaaaaa', 'camwwwww', 24682779393128);

/*
Voici la fonction du truand. Cette dernière permets de savoir combien d'itérations seront nécessaires afin d'atteindre le hash attendu.
Si le nombre en sortie est négatif alors chaîne attendue est en amont.
 */

function countPossible ( str )
{
    console.log(24682779393128 -  hash(str));
}

/* L'on sait dors et déjà que la chaîne à trouver se situt entre aaaaaaaa et cccccccc */
// countPossible('aaaaaaaa'); // 95423215681
// countPossible('cccccccc'); // -2145658039


/* Dès lors l'on va procéder à taton pour réduire la chaîne */
// countPossible('caaaaaaa'); // 491338548
// countPossible('cadddddd'); // 348798192
// countPossible('callllll'); //  63717480
// countPossible('callmmmm'); //  63665420
// countPossible('calnnnnn'); //  59865038
// countPossible('calwwwww'); //  46381491
// countPossible('calwwwww'); //  46381491
// countPossible('camaaaaa'); //  5930849
// countPossible('camcwwww'); //  3275788
// countPossible('camdwwww'); //  1401627
// countPossible('cameaaaa'); //   308366
// countPossible('cameiaaa'); //    55101
// countPossible('camelaaa'); //     4448
// countPossible('camelwww'); //   -16657

/* Ainsi avec un peu de logique et quelques intérations à la mano, l'on peut réduire
*  la plage entre 'camelaaa' et 'camelwww' avec 4448 tours de boucle. cqfd
*/
